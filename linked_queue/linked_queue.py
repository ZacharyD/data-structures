# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_queue.
#
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_15.py.

class LinkedQueue:
    def __init__(self):
        self.head = None
        self.tail = None

    def dequeue(self):
        value = self.head.value
        self.head = self.head.link
        if self.head is None:
            self.tail = None
        return value

    def enqueue(self, value):
        node = LinkedQueueNode(value)
        if self.head is None:
            self.head = node
            self.tail = node
        else:
            self.tail.link = node
            self.tail = node


class LinkedQueueNode:
    def __init__(self, value):
        self.value = value
        self.link = None

